import pytest

from incubation.models.user import User, Users


def test_user():
    pverkest = User(
        name="Pierre Verkest",
        login="pverkest",
        bcrypt_password="test"
    )
    assert pverkest.name == "Pierre Verkest"
    assert pverkest.login == "pverkest"
    assert pverkest.bcrypt_password == "test"


@pytest.mark.parametrize(
    "login,pwd,expected",
    [
        ("pverkest", "abc", "pverkest:abc"),
        (None, "abc", ":abc"),
    ]
)
def test_basic_auth(login, pwd, expected):
    pverkest = User(
        name="Please do not display name",
        login=login,
        bcrypt_password=pwd
    )
    assert pverkest.basic_auth_bcrypt == expected


def test_basic_auth_bcrypt_raises_if_no_bcrypt_pwd():
    pverkest = User(
        name="My name",
        login="test",
        bcrypt_password=None
    )
    with pytest.raises(ValueError) as err:
        _ = pverkest.basic_auth_bcrypt
        
    assert "Missing bcrypt_password retreiving basic bcrypt auth - User: My name (test)" in str(err.value)

def test_users():
    assert Users(
        users=[
            User(login="pverkest", bcrypt_password="abc"),
            User(login="averkest", bcrypt_password="test")
        ]
    ).basic_auth_bcrypt == "pverkest:abc,averkest:test"

def test_htpasswd_content():
    assert Users(
        users=[
            User(login="pverkest", bcrypt_password="abc"),
            User(login="averkest", bcrypt_password="test")
        ]
    ).htpasswd_content == "pverkest:abc\naverkest:test"
