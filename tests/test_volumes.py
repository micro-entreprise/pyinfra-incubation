
from incubation.models.volume import Volume, VolumeType, Bucket, BucketRegion


def test_volume():
    test = Volume(
        scw_volume_name="a",
        scw_volume_id="b",
        partition_uuid="c",
        mount_path="d",
    )
    assert test.scw_volume_name == "a"
    assert test.scw_volume_id == "b"
    assert test.partition_uuid == "c"
    assert test.mount_path == "d"
    assert test.fstab == (
        f"UUID={test.partition_uuid} "
        f"{test.mount_path} "
        "ext4 "
        "rw,discard,errors=remount-ro,x-systemd.growfs "
        "0 0"
    )


def test_volume_ro():
    test = Volume(
        scw_volume_name="a",
        scw_volume_id="b",
        partition_uuid="c",
        mount_path="d",
        partition_type=VolumeType.ext4,
        read_only=True,
    )
    assert test.scw_volume_name == "a"
    assert test.scw_volume_id == "b"
    assert test.partition_uuid == "c"
    assert test.mount_path == "d"
    assert test.fstab == (
        f"UUID={test.partition_uuid} "
        f"{test.mount_path} "
        "ext4 "
        "ro,discard,errors=remount-ro,x-systemd.growfs "
        "0 0"
    )

def test_directory():
    test = Volume(
        scw_volume_name="a",
        scw_volume_id="b",
        partition_uuid=None,
        mount_path="d",
        partition_type=VolumeType.directory
    )
    assert test.fstab == ""


def test_cifs():
    test = Volume(
        partition_uuid=None,
        remote_server_path="//1.1.1.1/dir",
        mount_path="d",
        partition_type=VolumeType.cifs
    )
    assert test.fstab == (
        f"{test.remote_server_path} {test.mount_path} "
        "cifs rw,_netdev,credentials=/root/.smb 0 0"
    )


def test_cifs_ro():
    test = Volume(
        partition_uuid=None,
        remote_server_path="//1.1.1.1/dir",
        mount_path="d",
        partition_type=VolumeType.cifs,
        read_only=True,
    )
    assert test.fstab == (
        f"{test.remote_server_path} {test.mount_path} "
        "cifs ro,_netdev,credentials=/root/.smb 0 0"
    )

def test_bucket():
    test = Bucket(
        name="a",
        scw_project_id="b",
        region=BucketRegion.PAR,
    )
    assert test.name == "a"
    assert test.scw_project_id == "b"
    assert test.region.value == "fr-par"
    assert test.region.name == "PAR"
    assert test.get_path() == "s3://a"
    assert test.get_path("test") == "s3://a/test"
    assert test.get_path("test", "file") == "s3://a/test/file"
