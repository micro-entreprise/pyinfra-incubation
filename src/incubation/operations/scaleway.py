from pyinfra.api import operation
from pyinfra.operations import files, server
from incubation.models.volume import Volume, VolumeType


@operation
def mount(volume: Volume, host=None, state=None):
    """Mount attached ext4 blok storage volume to a given path

    This basically ensure fstab entry is present and launch a `mount -a`
    afterwards.

    volume: Volume object with following attrs:
      * mount_path: path to mount the partiion
      * partition_uuid: partition UUID (use lsblk -o NAME,UUID,SIZE,RO,MOUNTPOINT)
      * scw_volume_name: Name of the volume used for messages

    if partition_uuid is not set we consider there are actually no volume to mount
    only ensure directory exists.

    TODO: prevent misconfig Volume object
    TODO: manage unmount
    TODO: manage more parameters
    """
    if volume.partition_type is VolumeType.directory:
        yield files.directory(
            volume.mount_path,
            host=host,
            state=state,
        )
    else:
        is_mounted = volume.mount_path in host.fact.mounts

        if not is_mounted:
            yield files.directory(
                volume.mount_path,
                host=host,
                state=state,
            )
            yield files.line(
                "/etc/fstab",
                volume.fstab,
                host=host,
                state=state,
            )
            yield server.shell(
                ["mount -a -v"],
                host=host,
                state=state,
            )
        else:
            host.noop(
                f"filesystem {volume.scw_volume_name} ({volume.mount_path}) is "
                "already mounted"
            )
