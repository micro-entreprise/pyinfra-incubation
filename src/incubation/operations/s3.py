"""The s3 module handles s3 storage object state.
Current supported command:

* s3 get

We assume s3cmd tool is present in path.
"""
from pyinfra.api import operation, MaskString, StringCommand
from pyinfra.api.connectors.util import escape_unix_path
from pyinfra.operations.util.files import chmod, chown
from datetime import timedelta

# PV: figure out what the purpose of pipeline_facts?
@operation(pipeline_facts={
    'file': 'dest',
})
def get(
    src,
    dest,
    s3_access_key=None,
    s3_secret_key=None,
    s3_host=None,
    s3_host_bucket=None,
    s3_config_file=None,
    user=None,
    group=None,
    mode=None,
    cache_time=None,
    force=False, 
    sha256sum=None,
    sha1sum=None,
    md5sum=None,
    state=None,
    host=None,
):
    """Download file from s3 bucket using s3cmd
    
    Inspired from pytinfra.operations.files.download

    Following s3cmd params are not required as you can choose to set
    it through ~/.s3cfg config file

    :param s3_access_key: S3 access key
    :param s3_secret_key: S3 secret key
    :param s3_host: S3 host (ie: `s3.fr-par.scw.cloud`)
    :param s3_host_bucket: S3 bucket template (ie: `"%(bucket)s.s3.fr-par.scw.cloud"`)
    :param s3_config_file: path to the `.s3cfg` file if not in `~/.s3cfg`

    """
    dest = escape_unix_path(dest)
    info = host.fact.file(dest)
        # Destination is a directory?
    if info is False:
        raise OperationError(
            'Destination {0} already exists and is not a file'.format(dest),
        )

    # Do we download the file? Force by default
    download = force

    # Doesn't exist, lets download it
    if info is None:
        download = True

    # Destination file exists & cache_time: check when the file was last modified,
    # download if old
    else:
        if cache_time:
            # Time on files is not tz-aware, and will be the same tz as the server's time,
            # so we can safely remove the tzinfo from host.fact.date before comparison.
            cache_time = host.fact.date.replace(tzinfo=None) - timedelta(seconds=cache_time)
            if info['mtime'] and info['mtime'] < cache_time:
                download = True

        if sha1sum:
            if sha1sum != host.fact.sha1_file(dest):
                download = True

        if sha256sum:
            if sha256sum != host.fact.sha256_file(dest):
                download = True

        if md5sum:
            if md5sum != host.fact.md5_file(dest):
                download = True

    if download:
        cmd = ["s3cmd"]
        if s3_config_file:
            cmd.extend(["-c", s3_config_file])
        if s3_host:
            cmd.extend(["--host", s3_host, ])
        if s3_host_bucket:
            cmd.extend(["--host-bucket", s3_host_bucket])
        if s3_access_key:
            cmd.extend(["--access_key", s3_access_key])
        if s3_secret_key:
            cmd.extend(["--secret_key", MaskString(s3_secret_key)])
        cmd.extend(["--force", "get", src, dest])
        s3cmd_command = StringCommand(*cmd)
        
        if host.fact.which('s3cmd'):
            yield s3cmd_command
        else:
            raise OperationError(
                's3cmd program not found'
            )

        if user or group:
            yield chown(dest, user, group)

        if mode:
            yield chmod(dest, mode)

        if sha1sum:
            yield (
                '((sha1sum {0} 2> /dev/null || shasum {0} || sha1 {0}) | grep {1}) '
                '|| (echo "SHA1 did not match!" && exit 1)'
            ).format(dest, sha1sum)

        if sha256sum:
            yield (
                '((sha256sum {0} 2> /dev/null || shasum -a 256 {0} || sha256 {0}) | grep {1}) '
                '|| (echo "SHA256 did not match!" && exit 1)'
            ).format(dest, sha256sum)

        if md5sum:
            yield (
                '((md5sum {0} 2> /dev/null || md5 {0}) | grep {1}) '
                '|| (echo "MD5 did not match!" && exit 1)'
            ).format(dest, md5sum)

    else:
        host.noop('file {0} has already been downloaded'.format(dest))