from dataclasses import dataclass


@dataclass
class SambaShare:
    part_name: str
    path: str
    comment: str = ""
    browseable: str = "no"
    read_only: str = "no"
    guest_ok: str = "no"
    valid_users: str = "@smb_group"
    force_group: str = "smb_group"
    force_directory_mode: int = 2770

    @property
    def config(self):
        return (
            f"[{self.part_name}]\n"
            f"   comment = {self.comment}\n"
            f"   path = {self.path}\n"
            f"   browseable = {self.browseable}\n"
            f"   read only = {self.read_only}\n"
            f"   guest ok = {self.guest_ok}\n"
            f"   valid users = {self.valid_users}\n"
            f"   force group = {self.force_group}\n"
            f"   force directory mode = {self.force_directory_mode}\n"
        )
