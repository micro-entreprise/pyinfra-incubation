from dataclasses import dataclass
from typing import List


@dataclass
class User:
    name: str = None
    login: str = None
    bcrypt_password: str = None
    ssha_password: str = None
    sha512: str = None
    groups: List[str] = None
    public_keys: List[str] = None

    @property
    def basic_auth_bcrypt(self):
        if not self.bcrypt_password:
            raise ValueError(
                f"Missing bcrypt_password retreiving basic bcrypt auth - "
                f"User: {self.name} ({self.login})"
            )
        return f"{self.login if self.login else ''}:{self.bcrypt_password}"

    @property
    def etc_shadow_line(self):
        """
        https://linuxize.com/post/etc-shadow-file/
        mark:$6$.n.:17736:0:99999:7:::
        [--] [----] [---] - [---] ----
        |      |      |   |   |   |||+-----------> 9. Unused
        |      |      |   |   |   ||+------------> 8. Expiration date
        |      |      |   |   |   |+-------------> 7. Inactivity period
        |      |      |   |   |   +--------------> 6. Warning period
        |      |      |   |   +------------------> 5. Maximum password age
        |      |      |   +----------------------> 4. Minimum password age
        |      |      +--------------------------> 3. Last password change
        |      +---------------------------------> 2. Encrypted Password
        +----------------------------------------> 1. Username

        if we needs to add epoch datetime.now().strftime("%s")
        """
        return f"{self.login}:{self.sha512}:19000:0:99999:7:::"

@dataclass
class Users:

    users: List[User] = None

    @property
    def basic_auth_bcrypt(self):
        return ",".join([user.basic_auth_bcrypt for user in self.users])
    
    @property
    def htpasswd_content(self):
        return "\n".join([user.basic_auth_bcrypt for user in self.users])
