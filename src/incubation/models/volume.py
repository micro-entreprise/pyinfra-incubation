import os
from dataclasses import dataclass, field
from enum import Enum
from os import path

from scaleway.instance.v1 import InstanceV1API
from scaleway.block.v1alpha1 import BlockV1Alpha1API




DEFAULT_SCW_PROJECT: str = os.getenv("DEFAULT_SCW_PROJECT", "")

class VolumeType(Enum):
    ext4 = "ext4"
    cifs = "cifs"
    directory = "directory"

    @property
    def fstab_template(self):
        """Return format python string template to generate fstab entry"""
        # have to compile fuse s3fs in order to make it
        # works: https://www.scaleway.com/en/docs/object-storage-with-s3fs/
        # fstab_line = None
        # if volume.bucket:
        #     fstab_line = (
        #         f"{volume.bucket} "
        #         f"{volume.mount_path} "
        #         "fuse.s3fs "
        #         f"_netdev,allow_other,use_path_request_style,"
        #         f"url=https://{volume.bucket}.s3.fr-{volume.bucket_region}.scw.cloud 0 0"
        #     )
        # else:
        # https://man7.org/linux/man-pages/man5/fstab.5.html
        templates= {
            "ext4": (
                "UUID={volume.partition_uuid} "
                "{volume.mount_path} "
                "ext4 "
                "{volume.fstab_access},discard,errors=remount-ro,x-systemd.growfs "
                "0 0"
            ),
            "cifs": (
                "{volume.remote_server_path} {volume.mount_path} "
                "cifs {volume.fstab_access},_netdev,credentials=/root/.smb 0 0"
            ),
            "directory": ""
        }
        return templates.get(self.name)

class BucketRegion(Enum):

    PAR = "fr-par"


@dataclass
class Volume:
    scw_project_id : str = DEFAULT_SCW_PROJECT
    scw_volume_name: str = None
    scw_volume_id: str = None
    partition_uuid: str = None
    remote_server_path: str = None
    mount_path: str = None
    partition_type: VolumeType = field(default=VolumeType.ext4)
    read_only: bool = False
    blk_storage: bool = False
    """blk_storage determine scaleway api to be use
    (block storage or instance)
    """

    @property
    def fstab(self) -> str:
        if self.partition_type.fstab_template:
            return self.partition_type.fstab_template.format(volume=self)
        else:
            return ""

    @property
    def fstab_access(self) -> str:
        return "ro" if self.read_only else "rw"

    def create_snapshot(self, scw_client, name):
        print("Snapshoting", self.scw_volume_name, "(", self.scw_volume_id,") as ", name)
        if self.blk_storage:
            scw_blk_api = BlockV1Alpha1API(scw_client)
            scw_blk_api.create_snapshot(
                volume_id=self.scw_volume_id,
                name=name,
                project_id=self.scw_project_id,
            )
        else:
            scw_api = InstanceV1API(scw_client)
            scw_api.create_snapshot(
                name=name,
                volume_id=self.scw_volume_id,
                # project_id=self.scw_project_id,
            )


@dataclass
class Bucket:
    scw_project_id : str = DEFAULT_SCW_PROJECT
    name: str = None
    region: BucketRegion = BucketRegion.PAR

    def get_path(self, *args):
        return path.join("s3://", self.name, *args)
