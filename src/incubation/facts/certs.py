import os
from pyinfra.api.facts import FactBase


class TraefikCertBase(FactBase):
    """Retreive certificates from traefik store
    
    As fare facts are read before orperations takes place, even
    you try to start your service before geting facts you may
    find an empty certificat as you probably to first start your service
    before getting new certificate.
    """

    EXT = {"certs": "crt", "private": "key"}
    kind = None

    def command(
        self,
        domain,
        traefik_store="/mnt/prod-traefik/certs/acme.json",
        traefik_store_dump="/mnt/prod-traefik/dumps_certs/",
        traefik_certs_dumper_image="ldez/traefik-certs-dumper:latest",
    ):
        # Find files in the given location
        pem = os.path.join(
            traefik_store_dump, self.kind, domain + "." + self.EXT[self.kind]
        )
        return (
            f"docker run --rm "
            f" -v {traefik_store}:/tmp/acme.json "
            f" -v {traefik_store_dump}:/tmp/dumps "
            f" {traefik_certs_dumper_image} "
            f"file --version v2 --source /tmp/acme.json --dest /tmp/dumps >> /dev/null "
            f"&& cat {pem} || /bin/true"
        )

    def process(self, output):
        return "\n".join(output)


class TraefikCert(TraefikCertBase):
    kind = "certs"


class TraefikKey(TraefikCertBase):
    kind = "private"
