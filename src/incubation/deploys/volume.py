
from pyinfra.api import deploy

from incubation.operations import scaleway
from incubation.models.volume import Volume

DEFAULTS = {}



@deploy("Mount volume", data_defaults=DEFAULTS)
def mount(
    volume: Volume,
    host=None,
    state=None,
):
    scaleway.mount(
        name=f"Mount {volume.scw_volume_name} partition to {volume.mount_path}",
        volume=volume,
        host=host,
        state=state,
    )