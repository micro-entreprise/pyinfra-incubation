from datetime import datetime
from pyinfra.api import deploy, DeployError
from incubation.models.user import User
from incubation.models.volume import Volume
from incubation.operations import scaleway
from pyinfra.operations import files, server
from incubation.operations import s3
from collections import namedtuple
from . import get_path

from os import path


DEFAULTS = {
    "odoo_image": "odoo",
    "odoo_image_version": "14.0",
    "odoo_pg_password": None,
    "odoo_pg_user": "odoo",
    "odoo_pg_db": "odoo",
    "odoo_workers": 4,
    "odoo_max_cron_threads": 2,
    "odoo_custom_config": None,
    "odoo_prod": False,
    "odoo_running_environment": None,
    "odoo_setup_module": "sale_managment",
    "odoo_master_password": None,
    "odoo_pg_docker_image": "postgres",
    "odoo_pg_docker_image_version": "13",
    "odoo_smtp_fake_user": None,
    "odoo_basic_auth_access": None,
    "odoo_with_queue_job": None,
    "odoo_queue_job_workers": 4,
    "odoo_queue_job_max_cron_threads": 0,
    "odoo_queue_job_channels": "root:3",
    "odoo_external_wkhtml_container": False,
    "odoo_server_wide_modules": "base,web",
}


@deploy("Install Odoo", data_defaults=DEFAULTS)
def install_odoo(
    volume: Volume,
    domain: str,
    s3_backup: str = None,
    s3_config_file: str = None,
    s3_access_key: str = None,
    s3_secret_key: str = None,
    s3_host: str = None,
    s3_host_bucket: str = None,
    extra_host_rule: str = None,
    state=None,
    host=None,
):
    """ Automat odoo deployements
    """
    if not host.data.odoo_pg_password:
        raise DeployError("You must define odoo_pg_password password")
    if not host.data.odoo_master_password:
        raise DeployError("You must define odoo_master_password password")

    scaleway.mount(
        name=f"Mount {volume.scw_volume_name} partition " f"to {volume.mount_path}",
        volume=volume,
        host=host,
        state=state,
    )

    wd = volume.mount_path
    compose = path.join(wd, "docker-compose.yml")
    odoo_conf = path.join(wd, "odoo.cfg")

    
    if host.data.odoo_running_environment:
        running_env = host.data.odoo_running_environment
    else:
        running_env = "production" if host.data.odoo_prod else "test"

    extra_config_lines = host.data.odoo_custom_config
    if not extra_config_lines:
        extra_config_lines = []

    config_file = namedtuple("FakePyinfraState", ["changed"])(changed=False)
    if not host.data.odoo_external_wkhtml_container:
        config_file = files.template(
            name=f"Odoo config",
            src=get_path("templates", "odoo.cfg.j2"),
            dest=odoo_conf,
            create_remote_dir=False,
            master_password=host.data.odoo_master_password,
            odoo_pg_password=host.data.odoo_pg_password,
            odoo_pg_user=host.data.odoo_pg_user,
            odoo_pg_db=host.data.odoo_pg_db,
            smtp_fake_enable=not host.data.odoo_prod,
            odoo_workers=host.data.odoo_workers,
            odoo_max_cron_threads=host.data.odoo_max_cron_threads,
            custom_config=extra_config_lines,
            host=host,
            state=state,
        )
    odoo_queue_job_config_file = namedtuple("FakePyinfraState", ["changed"])(changed=False)
    if host.data.odoo_with_queue_job and not host.data.odoo_external_wkhtml_container:
        odoo_queue_job_conf = path.join(wd, "odoo_queue_job.cfg")
        odoo_queue_job_config_file = files.template(
            name=f"Odoo config",
            src=get_path("templates", "odoo.queue_job.cfg.j2"),
            dest=odoo_queue_job_conf,
            create_remote_dir=False,
            odoo_pg_password=host.data.odoo_pg_password,
            odoo_pg_user=host.data.odoo_pg_user,
            odoo_pg_db=host.data.odoo_pg_db,
            smtp_fake_enable=not host.data.odoo_prod,
            odoo_workers=host.data.odoo_queue_job_workers,
            odoo_max_cron_threads=host.data.odoo_queue_job_max_cron_threads,
            custom_config=extra_config_lines,
            channels=host.data.odoo_queue_job_channels,
            host=host,
            state=state,
        )

    compose_file = files.template(
        name=f"Compose file Odoo",
        src=get_path("templates", "odoo.compose.j2"),
        dest=compose,
        create_remote_dir=False,
        domain=domain,
        odoo_image=host.data.odoo_image,
        odoo_image_version=host.data.odoo_image_version,
        running_env=running_env,
        master_password=host.data.odoo_master_password,
        odoo_workers=host.data.odoo_workers,
        odoo_max_cron_threads=host.data.odoo_max_cron_threads,
        odoo_queue_job_workers=host.data.odoo_queue_job_workers,
        odoo_queue_job_max_cron_threads=host.data.odoo_queue_job_max_cron_threads,
        channels=host.data.odoo_queue_job_channels,
        odoo_server_wide_modules=host.data.odoo_server_wide_modules,
        pg_docker_image=host.data.odoo_pg_docker_image,
        pg_docker_image_version=host.data.odoo_pg_docker_image_version,
        odoo_pg_password=host.data.odoo_pg_password,
        odoo_pg_user=host.data.odoo_pg_user,
        odoo_pg_db=host.data.odoo_pg_db,
        smtp_fake_enable=not host.data.odoo_prod,
        smtp_fake_user=host.data.odoo_smtp_fake_user,
        odoo_basic_auth=host.data.odoo_basic_auth_access,
        with_queue_job=host.data.odoo_with_queue_job,
        extra_host_rule=extra_host_rule,
        odoo_external_wkhtml_container=host.data.odoo_external_wkhtml_container,
        custom_config=extra_config_lines,
        host=host,
        state=state,
    )

    initialized_databases = path.join(wd, ".initialized_databases")
    # TODO instead reading databases file ensure ir_config_parameter exists
    # in the database (consider starting stack if docker compose is present)
    # data could be present in volume...
    fresh_install = True
    try:
        lines = host.fact.find_in_file(initialized_databases, host.data.odoo_pg_db)
        if lines:
            fresh_install = False
    except:
        pass

    
    if compose_file.changed or fresh_install or s3_backup:
        server.shell(
            name="Docker compose pull",
            commands=[f"docker compose -f {compose} pull"],
            host=host,
            state=state,
        )

        if s3_backup:
            host_path=path.join(wd, "pg_dumps/db_to_restore.dump")
            ct_path=path.join("/pg_dumps/", path.basename(host_path))
            
            files.directory(
                name='Ensure /pg_dumps exists',
                path=path.dirname(host_path),
                recursive=True,
                host=host,
                state=state,
            )
            s3.get(
                s3_backup,
                host_path,
                s3_access_key=s3_access_key,
                s3_secret_key=s3_secret_key,
                s3_host=s3_host,
                s3_host_bucket=s3_host_bucket,
                s3_config_file=s3_config_file,
                user=None,
                group=None,
                mode=None,
                # 6h
                cache_time=6 * 3600,
                force=False, 
                sha256sum=None,
                sha1sum=None,
                md5sum=None,
                state=state,
                host=host, 
            )
        # TODO: up maintenance
        server.shell(
            name="Docker compose down",
            commands=[f"docker compose -f {compose} down"],
            host=host,
            state=state,
        )
        volume.create_snapshot(
            host.data.scw_client,
            f"{datetime.now().isoformat()} - Before deploy odoo"
        )

        server.shell(
            name="Start database",
            commands=[f"docker compose -f {compose} up -d db"],
            host=host,
            state=state,
        )
        server.shell(
            name="Wait database",
            commands=[
                f"docker compose -f {compose} exec -T db bash -c \""
                f"while ! psql -U {host.data.odoo_pg_user} "
                f"-d {host.data.odoo_pg_db} -c "
                f"'select 1' ; do echo 'Waiting pgsql init...'; sleep 1; done;\""
            ],
            host=host,
            state=state,
        )
        if fresh_install and not s3_backup:
            files.directory(
                name='Ensure /filestore exists',
                path=path.join(wd, "filestore"),
                user='101',
                group='101',
                recursive=True,
                host=host,
                state=state,
            )
            server.shell(
                name="Install odoo",
                commands=[
                    f"docker compose -f {compose} run --rm odoo bash -c "
                    f"'odoo -c /etc/odoo.cfg "
                    f"--load-language fr_FR "
                    f"-i {host.data.odoo_setup_module} "
                    f"--stop-after-init --no-http'",
                ],
                host=host,
                state=state,
            )
        else:
            if s3_backup:
                server.shell(
                    name="restore dump",
                    commands=[
                        "docker compose "
                        f"-f {compose} " 
                        "exec -T db bash "
                        '-c "'
                        f"dropdb -U {host.data.odoo_pg_user} {host.data.odoo_pg_db}; "
                        f"createdb -U {host.data.odoo_pg_user} {host.data.odoo_pg_db} && "
                        f"pg_restore -U {host.data.odoo_pg_user} -Fc -O -d {host.data.odoo_pg_db} {ct_path} "
                        '"'
                    ],
                    host=host,
                    state=state,
                )
                # TODO allow to run SQL file
            # TODO remove assets bundel
            # delete from ir_attachment where res_model = 'ir.ui.view' and (name like '%.assets_%.js' or name like '%.assets_%.css');
            server.shell(
                name="Update odoo",
                commands=[
                    f"docker compose -f {compose} run --rm odoo bash -c "
                    f"'odoo -c /etc/odoo.cfg --stop-after-init -u all "
                    f"--i18n-overwrite --no-http'",
                ],
                host=host,
                state=state,
            )

        files.line(
            name="initialyzed database file",
            path=initialized_databases,
            line=host.data.odoo_pg_db,
            host=host,
            state=state,
        )
        if not host.data.odoo_prod:
            # TODO: ensure smtp_fake is configured and disable other smtp
            # docker compose exec db psql -d ${POSTGRES_DB} -U ${POSTGRES_USER} -c "
            pass

        server.shell(
            name="Start odoo stack",
            commands=[f"docker compose -f {compose} up -d"],
            host=host,
            state=state,
        )
        # TODO: down maintenance

        # TODO: recovery
            # restore previous snapshot

    else:
        force_restart = config_file.changed or odoo_queue_job_config_file.changed
        server.shell(
            name="Docker compose up",
            commands=[f"docker compose -f {compose} up -d {'--force-recreate' if force_restart else ''}"],
            host=host,
            state=state,
        )
