from pyinfra.api import deploy, DeployError
from pyinfra.operations import files, server
from incubation.models.volume import Volume
from incubation.operations import scaleway
from . import get_path

from os import path


DEFAULTS = {
    "mattermost_image": "mattermost/mattermost-team-edition",
    "mattermost_image_version": "release-7.10",
    "mattermost_postgres_image": "postgres",
    "mattermost_postgres_image_version": "15",
    "mattermost_pg_dbname": "mattermostdbname",
    "mattermost_pg_user": "mattermostdbuser",
    "mattermost_pg_password": "mattermostdbpwd",
}



@deploy("Install mattermost", data_defaults=DEFAULTS)
def install_mattermost(
    volume: Volume,
    mattermost_domain: str,
    state=None,
    host=None,
):

    scaleway.mount(
        name=f"Mount {volume.scw_volume_name} partition " f"to {volume.mount_path}",
        volume=volume,
        host=host,
        state=state,
    )

    wd = volume.mount_path
    compose = path.join(wd, "docker-compose.yml")
    mattermost_data = path.join(wd, "mattermost")
    files.directory(
        name="Ensure mattermost directory exists with proper access",
        path=mattermost_data,
        present=True,
        assume_present=False,
        user="2000",
        group="2000",
        recursive=True,
        host=host,
        state=state,
    )

    
    compose_file = files.template(
        name=f"Compose file mattermost",
        src=get_path("templates", "mattermost.compose.yml.j2"),
        dest=compose,
        create_remote_dir=False,
        mattermost_domain=mattermost_domain,
        mattermost_image=host.data.mattermost_image,
        mattermost_image_version=host.data.mattermost_image_version,
        postgres_image=host.data.mattermost_postgres_image,
        postgres_image_version=host.data.mattermost_postgres_image_version,
        mattermost_pg_dbname=host.data.mattermost_pg_dbname,
        mattermost_pg_user=host.data.mattermost_pg_user,
        mattermost_pg_password=host.data.mattermost_pg_password,
        host=host,
        state=state,
    )

    server.shell(
        name="Docker compose up",
        commands=[
            f"docker compose -f {compose} up -d "
            f"{'--force-recreate' if compose_file.changed else ''}"
        ],
        host=host,
        state=state,
    )
