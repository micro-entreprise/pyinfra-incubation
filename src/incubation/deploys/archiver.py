from os import path

from pyinfra.api import deploy
from pyinfra.operations import files, pip, systemd

from incubation.operations import scaleway
from incubation.models.volume import Volume

from . import get_path


DEFAULTS = {
    "s3cmd_config_path": "/root/.s3cfg",
    "archiver_venv_path": "/srv/venvs/archiver",
    "archiver_archives_sub_dir": "databases",
    "archiver_daily_dump_service_name": "archiver-daily-dump",
    "archiver_daily_dump_filter": '{"label": "docker-volume-dump.project"}',
    "archiver_daily_dump_on_calendar": "*-*-* 1,13:0:0",
    "anonymized_input": "databases",
    "anonymized_out": "anonymized-databases/",
    "anonymizer_service_name": "anonymizer",
    "anonymizer_on_calendar": "*-*-* 3:0:0",
    "postgres_docker_image": "registry.gitlab.com/dalibo/postgresql_anonymizer",
    "postgrges_docker_version": "stable",
    "prometheus_gateway_uri": None,
    "prometheus_gateway_password": None,
    "prometheus_gateway_username": None,
}

@deploy("Install DB timer to daily archive all", data_defaults=DEFAULTS)
def daily_dump(
    volume: Volume,
    host=None,
    state=None,
):
    # should I ensure python3-venv is installed here ?
    scaleway.mount(
        name=f"Mount {volume.scw_volume_name} partition " f"to {volume.mount_path}",
        volume=volume,
        host=host,
        state=state,
    )
    wd = volume.mount_path
    archive_dir = path.join(wd, host.data.archiver_archives_sub_dir)

    files.directory(
        name="Ensure backup directory exists",
        path=archive_dir,
        present=True,
        assume_present=False,
        user="666",
        group="666",
        mode="700",
        recursive=True,
        host=host,
        state=state,
    )

    install_docker_volume_dump = pip.packages(
        name='Install latest docker_volume_dump',
        packages=["pip", "wheel", "docker-volume-dump"],
        present=True,
        latest=True,
        requirements=None,
        pip='pip',
        virtualenv=host.data.archiver_venv_path,
        virtualenv_kwargs={
            "python": "python3",
            "venv": True,
            "site_packages": False,
            "always_copy": False,
            "present": True,
        }, 
        extra_install_args=None,
        host=host,
        state=state,
    )

    service_unit = files.template(
        name=f"Configure systemd { host.data.archiver_daily_dump_service_name }.service",
        src=get_path("templates", "archiver-daily-dump.service.j2"),
        dest=f"/etc/systemd/system/{ host.data.archiver_daily_dump_service_name }.service",
        user="root",
        group="root",
        mode="755",
        create_remote_dir=True,
        sudo=True,
        service_name=host.data.archiver_daily_dump_service_name,
        venv=host.data.archiver_venv_path,
        backup_dir=archive_dir,
        filter=host.data.archiver_daily_dump_filter,
        prom_gateway_uri=host.data.prometheus_gateway_uri,
        prom_gateway_pwd=host.data.prometheus_gateway_password,
        prom_gateway_user=host.data.prometheus_gateway_username,
        host=host,
        state=state,
    )
    timer_unit = files.template(
        name=f"Configure systemd {host.data.archiver_daily_dump_service_name}.timer",
        src=get_path("templates", "archiver-daily-dump.timer.j2"),
        dest=f"/etc/systemd/system/{ host.data.archiver_daily_dump_service_name }.timer",
        user="root",
        group="root",
        mode="755",
        create_remote_dir=True,
        sudo=True,
        service_name=host.data.archiver_daily_dump_service_name,
        venv=host.data.archiver_venv_path,
        backup_dir=archive_dir,
        filter=host.data.archiver_daily_dump_filter,
        on_calendar=host.data.archiver_daily_dump_on_calendar,
        host=host,
        state=state,
    )
    systemd.service(
        name=f"Ensure {host.data.archiver_daily_dump_service_name}.timer is enabled and running",
        service=f"{ host.data.archiver_daily_dump_service_name }.timer",
        running=True,
        # reloaded not applicable for *.timer unit use restart instead
        # reloaded=timer_unit.changed or service_unit.changed,
        restarted=timer_unit.changed or service_unit.changed,
        command=None,
        enabled=True,
        daemon_reload=timer_unit.changed or service_unit.changed,
        sudo=True,
        host=host,
        state=state,
    )


@deploy("Install DB timer to daily archive all", data_defaults=DEFAULTS)
def anonymizer_service(
    volume,
    local_sql_script_file,
    bucket=None,
    anonymized_input=None,
    anonymized_out=None,
    anonymizer_service_name=None,
    host=None,
    state=None
):
    """Get latest dump from a given directory and generate an anonymized database

    Assume postgresql-client installed on host machine
    """

    # should I ensure python3-venv is installed here ?
    scaleway.mount(
        name=f"Mount {volume.scw_volume_name} partition " f"to {volume.mount_path}",
        volume=volume,
        host=host,
        state=state,
    )
    anonymized_input = anonymized_input if anonymized_input is not None else host.data.anonymized_input
    anonymized_out = anonymized_out if anonymized_out is not None else host.data.anonymized_out
    anonymizer_service_name = anonymizer_service_name if anonymizer_service_name is not None else host.data.anonymizer_service_name
    
    
    wd = volume.mount_path
    out = path.join(wd, anonymized_out)
    outdir=path.dirname(out)
    input_ = path.join(wd, anonymized_input)
    compose = path.join(wd, f"docker-compose.{anonymizer_service_name}.yml")
    sql_script = path.join(wd, anonymizer_service_name + ".sql")
    bucket_path = None
    if bucket:
        bucket_path = bucket.get_path(anonymized_out)

    files.directory(
        name="Ensure out directory exists",
        path=outdir,
        present=True,
        assume_present=False,
        user="666",
        group="666",
        mode="700",
        recursive=True,
        host=host,
        state=state,
    )
    files.put(
        name="Update anonymized script",
        src=local_sql_script_file,
        dest=sql_script,
        user=None,
        group=None,
        mode=None,
        add_deploy_dir=True,
        create_remote_dir=True,
        force=False,
        assume_exists=False,
        host=host,
        state=state,
    )
    install_hosting = pip.packages(
        name='Install latest hosting',
        packages=["pip", "wheel", "hosting", "docker-compose", "s3cmd"],
        present=True,
        latest=True,
        requirements=None,
        pip='pip',
        virtualenv=host.data.archiver_venv_path,
        virtualenv_kwargs={
            "python": "python3",
            "venv": True,
            "site_packages": False,
            "always_copy": False,
            "present": True,
        }, 
        extra_install_args="--extra-index-url https://gitlab.com/api/v4/projects/25806119/packages/pypi/simple",
        host=host,
        state=state,
    )

    compose_file = files.template(
        name=f"Compose file: pgsl anonymizer",
        src=get_path("templates", "anonymizer.compose.yml.j2"),
        dest=compose,
        user="root",
        group="root",
        mode="600",
        create_remote_dir=False,
        image=host.data.postgres_docker_image,
        version=host.data.postgrges_docker_version,
        sql_script=sql_script,
        in_path=input_,
        out_dir=outdir,
        out_path=out,
        wd=wd,
        host=host,
        state=state,
    )
    service_unit = files.template(
        name=f"Configure systemd { anonymizer_service_name }.service",
        src=get_path("templates", "anonymizer.service.j2"),
        dest=f"/etc/systemd/system/{ anonymizer_service_name }.service",
        user="root",
        group="root",
        mode="755",
        create_remote_dir=True,
        sudo=True,
        service_name=anonymizer_service_name,
        venv=host.data.archiver_venv_path,
        compose=compose,
        in_path=input_,
        out_path=out,
        sql_script=sql_script,
        wd=wd,
        s3cfg_path=host.data.s3cmd_config_path,
        bucket_path=bucket_path,
        prom_gateway_uri=host.data.prometheus_gateway_uri,
        prom_gateway_pwd=host.data.prometheus_gateway_password,
        prom_gateway_user=host.data.prometheus_gateway_username,
        host=host,
        state=state,
    )
    timer_unit = files.template(
        name=f"Configure systemd {anonymizer_service_name}.timer",
        src=get_path("templates", "anonymizer.timer.j2"),
        dest=f"/etc/systemd/system/{ anonymizer_service_name }.timer",
        user="root",
        group="root",
        mode="755",
        create_remote_dir=True,
        sudo=True,
        service_name=anonymizer_service_name,
        venv=host.data.archiver_venv_path,
        compose=compose,
        on_calendar=host.data.anonymizer_on_calendar,
        in_path=input_,
        out_path=out,
        sql_script=sql_script,
        host=host,
        state=state,
    )
    systemd.service(
        name=f"Ensure {anonymizer_service_name}.timer is enabled and running",
        service=f"{ anonymizer_service_name }.timer",
        running=True,
        # reloaded not applicable for *.timer unit use restart instead
        # reloaded=timer_unit.changed or service_unit.changed,
        restarted=timer_unit.changed or service_unit.changed,
        command=None,
        enabled=True,
        daemon_reload=timer_unit.changed or service_unit.changed,
        sudo=True,
        host=host,
        state=state,
    )

