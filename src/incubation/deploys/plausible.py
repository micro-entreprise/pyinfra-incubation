from datetime import datetime
from pyinfra.api import deploy, DeployError
from incubation.models.user import User
from incubation.models.volume import Volume
from incubation.operations import scaleway
from pyinfra.operations import files, server
from incubation.operations import s3
from . import get_path

from os import path


DEFAULTS = {
    "plausible_image": "plausible/analytics",
    "plausible_image_version": "v1.5.1",
    "plausible_pg_password": None,
    "plausible_pg_user": "plausible",
    "plausible_pg_db": "plausible",
    "plausible_pg_docker_image": "postgres",
    "plausible_pg_docker_image_version": "14.6",
    "plausible_admin_user_email": "pierreverkest84@gmail.com",
    "plausible_admin_user_name": "Pierre Verkest",
    "plausible_secret_key_base": None,
    "plausible_mail_mailer_email": None,  # hello@plausible.local	The email id to use for as from address of all communications from Plausible.
    "plausible_mail_smtp_host_addr": None,  # localhost	The host address of your smtp server.
    "plausible_mail_smtp_host_port": None,  # The port of your smtp server.
    "plausible_mail_smtp_user_name": None,  # The username/email in case SMTP auth is enabled.
    "plausible_mail_smtp_user_pwd": None,  # The password in case SMTP auth is enabled.
    "plausible_mail_smtp_host_ssl_enabled": True,  # If SSL is enabled for SMTP connection
    "plausible_mail_smtp_retries": 2,  # Number of retries to make until mailer gives up.
}


@deploy("Install Plausible", data_defaults=DEFAULTS)
def install_plausible(
    volume: Volume,
    domain: str,
    state=None,
    host=None,
):
    """ Automat plausible deployements
    """

    if not host.data.plausible_secret_key_base:
        raise DeployError("You must define plausible_secret_key_base")

    scaleway.mount(
        name=f"Mount {volume.scw_volume_name} partition " f"to {volume.mount_path}",
        volume=volume,
        host=host,
        state=state,
    )

    wd = volume.mount_path
    compose = path.join(wd, "docker-compose.yml")

    compose_file = files.template(
        name=f"Compose file plausible",
        src=get_path("templates", "plausible.compose.j2"),
        dest=compose,
        create_remote_dir=False,
        domain=domain,
        plausible_image=host.data.plausible_image,
        plausible_image_version=host.data.plausible_image_version,
        pg_docker_image=host.data.plausible_pg_docker_image,
        pg_docker_image_version=host.data.plausible_pg_docker_image_version,
        plausible_pg_password=host.data.plausible_pg_password,
        plausible_pg_user=host.data.plausible_pg_user,
        plausible_pg_db=host.data.plausible_pg_db,
        plausible_admin_user_email=host.data.plausible_admin_user_email,
        plausible_admin_user_name=host.data.plausible_admin_user_name,
        plausible_secret_key_base=host.data.plausible_secret_key_base,
        with_smtp=all([
            host.data.plausible_mail_mailer_email,
            host.data.plausible_mail_smtp_host_addr,
            host.data.plausible_mail_smtp_host_port,
        ]),
        plausible_mail_mailer_email=host.data.plausible_mail_mailer_email,
        plausible_mail_smtp_host_addr=host.data.plausible_mail_smtp_host_addr,
        plausible_mail_smtp_host_port=host.data.plausible_mail_smtp_host_port,
        plausible_mail_smtp_user_name=host.data.plausible_mail_smtp_user_name,
        plausible_mail_smtp_user_pwd=host.data.plausible_mail_smtp_user_pwd,
        plausible_mail_smtp_host_ssl_enabled=host.data.plausible_mail_smtp_host_ssl_enabled,
        plausible_mail_smtp_retries=host.data.plausible_mail_smtp_retries,
        maxmindinc=False,
        host=host,
        state=state,
    )

    initialized_databases = path.join(wd, ".initialized_databases")
    # TODO instead reading databases file ensure ir_config_parameter exists
    # in the database (consider starting stack if docker compose is present)
    # data could be present in volume...
    fresh_install = True
    try:
        lines = host.fact.find_in_file(initialized_databases, host.data.plausible_pg_db)
        if lines:
            fresh_install = False
    except:
        pass


    if compose_file.changed or fresh_install:
        server.shell(
            name="Docker compose pull",
            commands=[f"docker-compose -f {compose} pull"],
            host=host,
            state=state,
        )
        server.shell(
            name="Docker compose down",
            commands=[f"docker-compose -f {compose} down"],
            host=host,
            state=state,
        )
        volume.create_snapshot(
            host.data.scw_client,
            f"{datetime.now().isoformat()} - Before deploy plausible"
        )
        server.shell(
            name="Start database",
            commands=[f"docker-compose -f {compose} up -d plausible_db"],
            host=host,
            state=state,
        )
        server.shell(
            name="Wait database",
            commands=[
                f"docker-compose -f {compose} exec -T plausible_db bash -c \""
                f"while ! psql -U {host.data.plausible_pg_user} "
                f"-d {host.data.plausible_pg_db} -c "
                f"'select 1' ; do echo 'Waiting pgsql init...'; sleep 1; done;\""
            ],
            host=host,
            state=state,
        )
        server.shell(
            name="Install plausible",
            commands=[
                f"docker-compose -f {compose} run --rm plausible sh -c "
                f"'/entrypoint.sh db createdb && /entrypoint.sh db migrate'"
            ],
            host=host,
            state=state,
        )
        files.line(
            name="initialized database file",
            path=initialized_databases,
            line=host.data.plausible_pg_db,
            host=host,
            state=state,
        )

    server.shell(
        name="Start plausible stack",
        commands=[f"docker-compose -f {compose} up -d"],
        host=host,
        state=state,
    )
