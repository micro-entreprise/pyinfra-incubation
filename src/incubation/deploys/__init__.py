from os import path


def get_path(kind, name):
    return path.join(
        path.dirname(__file__),
        kind,
        name,
    )

