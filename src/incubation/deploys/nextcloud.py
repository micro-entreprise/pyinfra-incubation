from datetime import datetime
from pyinfra.api import deploy, DeployError
from incubation.models.user import User
from incubation.models.volume import Volume
from incubation.operations import scaleway
from pyinfra.operations import files, server
from . import get_path

from os import path


DEFAULTS = {
    "traefik_network": "traefik",  # docker published container network
    "nextcloud_image": "nextcloud",
    "nextcloud_image_version": "20",
    "nextcloud_pg_password": None,
    "nextcloud_pg_user": "nextcloud",
    "nextcloud_pg_db": "nextcloud",
}


@deploy("Install Nextcloud", data_defaults=DEFAULTS)
def install_nextcloud(
    volume: Volume,
    domain: str,
    state=None,
    host=None,
):
    """ A étudier les modules proposé par défaut:
        * agenda
        * contacts
        * discussion
        * mail
        * onlyoffice
    """
    if not host.data.nextcloud_pg_password:
        raise DeployError("You must define nextcloud_pg_password password")

    scaleway.mount(
        name=f"Mount {volume.scw_volume_name} partition " f"to {volume.mount_path}",
        volume=volume,
        host=host,
        state=state,
    )

    wd = volume.mount_path
    compose = path.join(wd, "docker-compose.yml")
    opcache_config = path.join(wd, "opcache.ini")

    compose_file = files.template(
        name=f"opcache config",
        src=get_path("templates", "nextcloud.opcache.ini.j2"),
        dest=opcache_config,
        create_remote_dir=False,
        host=host,
        state=state,
    )

    compose_file = files.template(
        name=f"Compose file Nextcloud",
        src=get_path("templates", "nextcloud.compose.j2"),
        dest=compose,
        create_remote_dir=False,
        domain=domain,
        nextcloud_image=host.data.nextcloud_image,
        nextcloud_image_version=host.data.nextcloud_image_version,
        nextcloud_pg_password=host.data.nextcloud_pg_password,
        nextcloud_pg_user=host.data.nextcloud_pg_user,
        nextcloud_pg_db=host.data.nextcloud_pg_db,
        traefik_network=host.data.traefik_network,
        host=host,
        state=state,
    )
    if compose_file.changed:
        server.shell(
            name="Docker compose pull",
            commands=[f"docker compose -f {compose} pull"],
            host=host,
            state=state,
        )
        # TODO: up maintenance
        server.shell(
            name="Docker compose down",
            commands=[f"docker compose -f {compose} down"],
            host=host,
            state=state,
        )

        volume.create_snapshot(
            host.data.scw_client,
            f"{datetime.now().isoformat()} - Before deploy nextcloud"
        )
        server.shell(
            name="Docker compose up",
            commands=[f"docker compose -f {compose} up -d"],
            host=host,
            state=state,
        )
        server.shell(
            name="Wait 15s",
            commands=["sleep 15"],
            host=host,
            state=state,
        )
        
        server.shell(
           name="Wait Nextcloud ready to migrate",
           commands=[
                f"docker-compose -f {compose} exec -T --user www-data engine bash -c \'"
                f"./occ status\'"
           ],
           host=host,
           state=state,
        )
        server.shell(
            name="Migrate nextcloud",
            commands=[
                f"docker compose -f {compose} exec -T --user www-data engine bash -c \'"
                f"./occ upgrade "
                f"&& ./occ maintenance:mode --on "
                f"&& ./occ db:add-missing-columns "
                f"&& ./occ db:add-missing-primary-keys "
                f"&& ./occ db:add-missing-indices "
                f"&& ./occ maintenance:repair --include-expensive "
                f"&& ./occ maintenance:mode --off "
                f"&& ./occ db:convert-filecache-bigint -n -v "
                f"&& ./occ app:update --all "
                f"&& ./occ maintenance:mode --off "
                f"\'"
            ],
            host=host,
            state=state,
        )
        # TODO: down maintenance

        # TODO: recovery
            # restore previous snapshot

    else:
        server.shell(
            name="Docker compose up",
            commands=[f"docker compose -f {compose} up -d"],
            host=host,
            state=state,
        )
