from pyinfra.api import deploy, DeployError
from incubation.models.user import User
from incubation.models.volume import Volume
from incubation.operations import scaleway
from pyinfra.operations import files, server
from . import get_path

from os import path


DEFAULTS = {
    "openldap_image": "anybox/openldap",
    "openldap_image_tag": "2.4.50",
    "changeme_image": "nginx",
    "changeme_image_tag": "latest",
    "traefik_cert_path": "/mnt/prod-traefik/certs/acme.json",
    "ldap_certificate": "cert.crt",
    "ldap_certificate_key": "cert.key",
    "ldap_ca_certificate": "ca.crt",
}


@deploy("Install OpenLDAP", data_defaults=DEFAULTS)
def install_ldap(
    volume: Volume,
    domain_organization: str,
    organization: str,
    admin_user: User,
    domain: str = None,
    ldap_root_password: str = "{SSHA}B031mzW+AgUgM/eswBWmTyIopsR8lV5d",
    state=None,
    host=None,
):
    if not domain:
        domain = "ldap." + domain_organization

    scaleway.mount(
        name=f"Mount {volume.scw_volume_name} partition " f"to {volume.mount_path}",
        volume=volume,
        host=host,
        state=state,
    )

    wd = volume.mount_path
    ssl_directory = path.join(wd, "ssl")
    backend_directory = path.join(wd, "ldap_backend", "openldap-data")
    cert = path.join(ssl_directory, host.data.ldap_certificate)
    ca_cert = path.join(ssl_directory, host.data.ldap_ca_certificate)
    key = path.join(ssl_directory, host.data.ldap_certificate_key)
    compose = path.join(wd, "docker-compose.yml")

    compose_file = files.template(
        name=f"Compose file LDAP",
        src=get_path("templates", "ldap.compose.j2"),
        dest=compose,
        create_remote_dir=False,
        openldap_image=host.data.openldap_image,
        openldap_image_tag=host.data.openldap_image_tag,
        changeme_image=host.data.changeme_image,
        changeme_image_tag=host.data.changeme_image_tag,
        domain=domain,
        domain_organization=domain_organization,
        organization=organization,
        ldap_root_password=ldap_root_password,
        ldap_default_admin_uid=admin_user.login,
        ldap_default_admin_password=admin_user.ssha_password,
        ldap_certificate=host.data.ldap_certificate,
        ldap_certificate_key=host.data.ldap_certificate_key,
        ldap_ca_certificate=host.data.ldap_ca_certificate,
        host=host,
        state=state,
    )

    files.directory(
        name="Ensure access right on backend directory",
        path=backend_directory,
        present=True,
        assume_present=False,
        user="666",
        group="666",
        mode="700",
        recursive=True,
        host=host,
        state=state,
    )

    # ensure changeme is started to generate cert from traefik for given domain name
    server.shell(
        name="Docker compose pull",
        commands=[f"docker compose -f {compose} up -d changeme"],
        host=host,
        state=state,
    )

    server.shell(
        name="Docker compose up",
        commands=[f"docker compose -f {compose} up -d changeme"],
        host=host,
        state=state,
    )

    # TODO wait service return 200 status with a timeout available to let time
    # to generate certifcate
    # curl -s -o /dev/null -I -w "%{http_code}" https://traefik.prod.verkest.fr
    # In the mean time we don't really care if cert is not preset deploy
    # will failled, just wait and launch again
    store_dump = path.join(
            path.abspath(
                path.join(path.dirname(host.data.traefik_cert_path), "..")
            ),
            "dumps_certs"
        )
    crt = host.fact.traefik_cert(
        domain,
        traefik_store=host.data.traefik_cert_path,
        traefik_store_dump=store_dump,
    )
    crt_key = host.fact.traefik_key(
        domain,
        traefik_store=host.data.traefik_cert_path,
        traefik_store_dump=store_dump,
    )
    if crt and key:
        cert_file = files.template(
            name="Set certificat",
            src=get_path("templates", "empty_content.j2"),
            dest=cert,
            mode="500",
            user="666",
            group="666",
            content=crt,
            host=host,
            state=state,
        )
        ca_cert_file = files.template(
            name="Set CA certificat",
            src=get_path("templates", "empty_content.j2"),
            dest=ca_cert,
            mode="500",
            user="666",
            group="666",
            content=crt,
            host=host,
            state=state,
        )
        key_file = files.template(
            name="Set certificat",
            src=get_path("templates", "empty_content.j2"),
            dest=key,
            mode="500",
            user="666",
            group="666",
            content=crt_key,
            # content=ssl.DER_cert_to_PEM_cert(crt["key"].encode()),
            host=host,
            state=state,
        )
        files.directory(
            name="Ensure access right on ssl directory",
            path=ssl_directory,
            present=True,
            assume_present=False,
            user="666",
            group="666",
            mode="500",
            recursive=True,
            host=host,
            state=state,
        )
        force_recreate = cert_file.changed or key_file.changed or key_file.changed
        server.shell(
            name="Docker compose up",
            commands=[f"docker compose -f {compose} up -d {'--force-recreate' if force_recreate else ''}"],
            host=host,
            state=state,
        )
