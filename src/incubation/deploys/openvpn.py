"""Those states are used to configure SSH tunnel between
an SSHD server (with accessible public IP) and a client.

If a "client" is behind an ISP which do not provide public IP as
it can be with 4G/G5/Satelit router device. Those states provide
configuration to launch an sshd deamon (in a docker container)
on your server with allowing tunneling configuration.

A systemd service with robust failed tolerance to open ssh tunnel
and transmit a list of port

"""
from os import path

from pyinfra.api import deploy, DeployError


from pyinfra.operations import files, server, systemd, apt
from incubation.operations import scaleway

from incubation.models.volume import Volume
from . import get_path

DEFAULTS = {
}


@deploy("Install openvpn client (systemd service on host system)", data_defaults=DEFAULTS)
def openvpn_client(
    vpn_name,
    ovpn_config_file,
    ovpn_pass_file,
    host=None,
    state=None,
):
    
    apt.packages(
        name="Install openvpn",
        packages=["openvpn"],
        state=state,
        host=host,
    )

    service_name = f"vpn-{vpn_name}"
    ovpn_path = f"/etc/vpn/{service_name}.ovpn"
    ovpn_pass_file_path = f"/etc/vpn/{service_name}pass.txt"
    ovpn_file = files.put(
        name="Update vpn config file",
        src=ovpn_config_file,
        dest=ovpn_path,
        user="root",
        group="root",
        mode=600,
        add_deploy_dir=True,
        create_remote_dir=True,
        force=False,
        assume_exists=False,
        host=host,
        state=state,
    )
    pass_file = files.put(
        name="Update vpn pass file",
        src=ovpn_pass_file,
        dest=ovpn_pass_file_path,
        user="root",
        group="root",
        mode=600,
        add_deploy_dir=True,
        create_remote_dir=True,
        force=False,
        assume_exists=False,
        host=host,
        state=state,
    )
    
    service_unit = files.template(
        name=f"Configure systemd {service_name}.service",
        src=get_path("templates", "openvpn.service.j2"),
        dest=f"/etc/systemd/system/{service_name}.service",
        user="root",
        group="root",
        mode="600",
        create_remote_dir=True,
        sudo=True,
        service_name=service_name,
        ovpn_path=ovpn_path,
        ovpn_pass_file_path=ovpn_pass_file_path,
        host=host,
        state=state,
    )
    systemd.service(
        name=f"Ensure {service_name}.service is enabled and running",
        service=f"{service_name}.service",
        running=True,
        # reloaded not applicable for *.timer unit use restart instead
        # reloaded=timer_unit.changed or service_unit.changed,
        restarted=service_unit.changed or ovpn_file.changed or pass_file.changed,
        command=None,
        enabled=True,
        daemon_reload=service_unit.changed,
        sudo=True,
        host=host,
        state=state,
    )



