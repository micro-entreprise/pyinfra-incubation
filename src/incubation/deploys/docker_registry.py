from os import path

from pyinfra.api import deploy, DeployError


from pyinfra.operations import files, server
from incubation.operations import scaleway

from incubation.models.volume import Volume
from . import get_path

DEFAULTS = {
    "traefik_network": "traefik",  # docker published container network
    "docker_registry_image": "registry",
    "docker_registry_image_version": "2",
    "docker_registry_s3_access_key": None,
    "docker_registry_s3_secret_key": None,
    "docker_registry_s3_bucket": None,
    "docker_registry_htpasswd": None,
    "docker_registry_s3_region": "fr-par",
    "docker_registry_s3_regionendpoint": "https://s3.fr-par.scw.cloud",
    "docker_registry_service_name": "docker-regsistry",  # traefik service name
}


@deploy("Install Docker registry", data_defaults=DEFAULTS)
def install_docker_registry(
    volume: Volume,
    domain: str,
    state=None,
    host=None,
):

    if not host.data.docker_registry_s3_access_key:
        raise DeployError("You must define docker_registry_s3_access_key")

    if not host.data.docker_registry_s3_secret_key:
        raise DeployError("You must define docker_registry_s3_secret_key")

    if not host.data.docker_registry_s3_bucket:
        raise DeployError("You must define docker_registry_s3_bucket")


    if not host.data.docker_registry_htpasswd:
        raise DeployError("You must define docker_registry_htpasswd")

    scaleway.mount(
        name=f"Mount {volume.scw_volume_name} partition " f"to {volume.mount_path}",
        volume=volume,
        host=host,
        state=state,
    )

    wd = volume.mount_path
    compose = path.join(wd, "docker-compose.yml")
    conf = path.join(wd, "registry.yml")
    htpasswd = path.join(wd, "registry_htpasswd")

    config_file = files.template(
        name=f"Docker registry config",
        src=get_path("templates", "docker-registry.yml.j2"),
        dest=conf,
        user="root",
        group="root",
        mode="600",
        s3_access_key=host.data.docker_registry_s3_access_key,
        s3_secret_key=host.data.docker_registry_s3_secret_key,
        s3_bucket=host.data.docker_registry_s3_bucket,
        s3_region=host.data.docker_registry_s3_region,
        s3_regionendpoint=host.data.docker_registry_s3_regionendpoint,
        host=host,
        state=state,
    )

    htpasswd_file = files.template(
        name=f"Docker registry htpasswd config",
        src=get_path("templates", "empty_content.j2"),
        dest=htpasswd,
        user="root",
        group="root",
        mode="600",
        content=host.data.docker_registry_htpasswd,
        host=host,
        state=state,
    )
    compose_file = files.template(
        name=f"Compose file: docker registry",
        src=get_path("templates", "docker-registry.compose.j2"),
        dest=compose,
        user="root",
        group="root",
        mode="600",
        create_remote_dir=False,
        image=host.data.docker_registry_image,
        version=host.data.docker_registry_image_version,
        traefik_network=host.data.traefik_network,
        domain=domain,
        service_name=host.data.docker_registry_service_name,
        host=host,
        state=state,
    )

    force_recreate = ""
    if config_file.changed or htpasswd_file:
        force_recreate = "--force-recreate "
    server.shell(
        name="Docker compose up",
        commands=[f"docker compose -f {compose} up --build -d {force_recreate}"],
        host=host,
        state=state,
    )
