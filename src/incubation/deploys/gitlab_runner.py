"""Once this is deployed you have to register the runner
with gilab: https://docs.gitlab.com/runner/register/index.html
""" 
from os import path

from pyinfra.api import deploy, DeployError
from pyinfra.operations import files, server, apt

from . import get_path

DEFAULTS = {
    "traefik_network": "traefik",
    "gitlab_runner_docker_image": "gitlab/gitlab-runner",
    "gitlab_runner_docker_image_tag": "latest",
    "gitlab_runner_service": "gitlab-runner",
    "gitlab_runner_url": "https://gitlab.com",
    "gitlab_runner_name": None,
    "gitlab_runner_token": None,
    "gitlab_runner_shell_config":"/etc/gitlab-runner/config.toml",
}


@deploy(
    "Install gitlab runner docker executor (manual registration)",
    data_defaults=DEFAULTS
)
def gitlab_runner(
    state=None,
    host=None,
):
    wd = path.join("/srv", host.data.gitlab_runner_service)
    compose = path.join(wd, "docker-compose.yml")
    conf = path.join(wd, "config.toml")
    name = host.data.gitlab_runner_name
    if not name:
        name = host.data.gitlab_runner_service

    if not host.data.gitlab_runner_token:
        raise DeployError("You must define gitlab_runner_token token")
    
    files.directory(
        name=f"Ensure {host.data.gitlab_runner_service} directory exists",
        path=wd,
        present=True,
        assume_present=False,
        user="root",
        group="root",
        mode="600",
        recursive=False,
        host=host,
        state=state,
    )
    config_file = files.template(
        name=f"Gitlab runner config",
        src=get_path("templates", "gitlab-runner.config.toml.j2"),
        dest=conf,
        user="root",
        group="root",
        mode="600",
        runner_name=host.data.gitlab_runner_name,
        url=host.data.gitlab_runner_url,
        token=host.data.gitlab_runner_token,
        host=host,
        state=state,
    )
    compose_file = files.template(
        name=f"Compose file: {host.data.gitlab_runner_service}",
        src=get_path("templates", "gitlab-runner.compose.yml.j2"),
        dest=compose,
        user="root",
        group="root",
        mode="600",
        create_remote_dir=False,
        image=host.data.gitlab_runner_docker_image,
        version=host.data.gitlab_runner_docker_image_tag,
        host=host,
        state=state,
    )
    up_service = server.shell(
        name="Docker compose up",
        commands=[
            f"docker compose -f {compose} pull; "
            f"docker compose -f {compose} up -d {'--force-recreate' if config_file.changed else ''}"
        ],
        host=host,
        state=state,
    )


@deploy(
    "Install gitlab runner shell executor (manual registration)",
    data_defaults=DEFAULTS
)
def gitlab_runner_shell_executor(
    state=None,
    host=None,
):

    apt.packages(
        name="Install apt requirements to use HTTPS",
        packages=["debian-archive-keyring", "apt-transport-https", "ca-certificates", "gnupg"],
        state=state,
        host=host,
    )

    lsb_release = host.fact.lsb_release
    lsb_id = lsb_release["id"].lower()

    apt.key(
        name="Download the Docker apt key",
        src="https://packages.gitlab.com/runner/gitlab-runner/gpgkey",
        state=state,
        host=host,
    )

    add_apt_repo = apt.repo(
        name="Add the Docker apt repo",
        src=(
            "deb https://packages.gitlab.com/runner/gitlab-runner/{0} {1} main\n"
            "deb-src https://packages.gitlab.com/runner/gitlab-runner/{0} {1} main"
        ).format(
            lsb_id, lsb_release["codename"],
            lsb_id, lsb_release["codename"],
        ),
        filename="gitlab-runner",
        state=state,
        host=host,
    )

    gitlab_runner_install = apt.packages(
        name="Install Gitlab runner via apt",
        packages="gitlab-runner",
        update=add_apt_repo.changed,  # update if we added the repo
        state=state,
        host=host,
    )