from os import path

from pyinfra.api import deploy, DeployError


from pyinfra.operations import systemd, apt
from incubation.operations import scaleway

from incubation.models.volume import Volume
from . import get_path

DEFAULTS = {}


@deploy("Install and configure fail2ban", data_defaults=DEFAULTS)
def fail2ban(
    state=None,
    host=None,
):
    # assume apt list is updated
    apt.packages(
        name="Ensure fail2ban is installed",
        packages=["fail2ban"],
        state=state,
        host=host,
    )

    systemd.service(
        name=f"Ensure fail2ban.service is enabled and running",
        service="fail2ban.service",
        running=True,
        # reloaded not applicable for *.timer unit use restart instead
        # reloaded=timer_unit.changed or service_unit.changed,
        # restarted=timer_unit.changed or service_unit.changed,
        # daemon_reload=timer_unit.changed or service_unit.changed,
        command=None,
        enabled=True,
        sudo=True,
        host=host,
        state=state,
    )
