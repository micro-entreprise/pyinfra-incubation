
from os import path

from pyinfra.api import deploy, DeployError
from pyinfra.operations import files, server

from . import get_path

DEFAULTS = {
    "traefik_network": "traefik",
}


@deploy("Install stateless service", data_defaults=DEFAULTS)
def stateless_service(
    domain: str,
    docker_image: str,
    docker_image_versoin: str,
    basic_auth=None,
    auth=None,
    state=None,
    host=None,
):
    if auth is not None and auth not in ("basic", "login"):
        raise DeployError("auth is not valid, must be one of: `basic` or `login` received {auth}")

    wd = path.join("/srv", domain)
    compose = path.join(wd, "docker-compose.yml")
    html = path.join(wd, "html")

    files.directory(
        name=f"Ensure {domain} directory exists",
        path=wd,
        present=True,
        assume_present=False,
        user="root",
        group="101",  # allow external user to access
        mode="650",
        recursive=False,
        host=host,
        state=state,
    )
    files.directory(
        name=f"Ensure {domain} html directory exists",
        path=html,
        present=True,
        assume_present=False,
        user="101",  # nginx
        group="101",  # nginx
        mode="775",
        recursive=True,
        host=host,
        state=state,
    )
    compose_file = files.template(
        name=f"Compose file: {domain}",
        src=get_path("templates", "stateless.compose.yml.j2"),
        dest=compose,
        user="root",
        group="root",
        mode="600",
        create_remote_dir=False,
        image=docker_image,
        version=docker_image_versoin,
        domain=domain,
        auth=auth,
        basic_auth=basic_auth,
        service_name=domain.replace(".", "-"),
        traefik_network=host.data.traefik_network,
        host_volume_path=html,
        host=host,
        state=state,
    )
    up_service = server.shell(
        name="Docker compose up",
        commands=[
            f"docker compose -f {compose} pull; docker compose -f {compose} up -d"
        ],
        host=host,
        state=state,
    )
