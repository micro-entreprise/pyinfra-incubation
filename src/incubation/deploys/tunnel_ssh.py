"""Those states are used to configure SSH tunnel between
an SSHD server (with accessible public IP) and a client.

If a "client" is behind an ISP which do not provide public IP as
it can be with 4G/G5/Satelit router device. Those states provide
configuration to launch an sshd deamon (in a docker container)
on your server with allowing tunneling configuration.

A systemd service with robust failed tolerance to open ssh tunnel
and transmit a list of port

"""
from os import path

from pyinfra.api import deploy, DeployError


from pyinfra.operations import files, server, systemd
from incubation.operations import scaleway

from incubation.models.volume import Volume
from . import get_path

DEFAULTS = {
    "sshd_image": "docker.io/panubo/sshd",
    "sshd_image_version": "latest",
}


@deploy("Install tunnel server (sshd deamon in docker container)", data_defaults=DEFAULTS)
def tunnel_ssh_server(
    volume,
    ports,
    local_authorized_keys_file,
    host=None,
    state=None,
):
    scaleway.mount(
        name=f"Mount {volume.scw_volume_name} partition " f"to {volume.mount_path}",
        volume=volume,
        host=host,
        state=state,
    )

    wd = volume.mount_path
    compose = path.join(wd, "docker-compose.yml")
    authorized_keys_file = path.join(wd, "authorized_keys")
    sshd_config_file = path.join(wd, "sshd_config")
    authkey = files.put(
        name="Update authorized_keys",
        src=local_authorized_keys_file,
        dest=authorized_keys_file,
        user=1000,
        group=1000,
        mode=600,
        add_deploy_dir=True,
        create_remote_dir=True,
        force=False,
        assume_exists=False,
        host=host,
        state=state,
    )

    sshd_config = files.template(
        name="Update sshd_config",
        src=get_path("templates", "tunnel-sshd-config.j2"),
        dest=sshd_config_file,
        user="root",
        group="root",
        mode=644,
        service_name="sshd-tunnel-server",
        host=host,
        state=state,
    )
    compose_file = files.template(
        name=f"Compose file: sshd",
        src=get_path("templates", "tunnel-ssh.compose.j2"),
        dest=compose,
        user="root",
        group="root",
        mode="600",
        create_remote_dir=False,
        image=host.data.sshd_image,
        version=host.data.sshd_image_version,
        ports=ports,
        service_name="sshd-tunnel-server",
        host=host,
        state=state,
    )

    force_recreate = ""
    if compose_file.changed or authkey.changed or sshd_config.changed:
        force_recreate = "--force-recreate "
    server.shell(
        name="Docker compose up",
        commands=[f"docker compose -f {compose} up --build -d {force_recreate}"],
        host=host,
        state=state,
    )


@deploy("Install tunnel client (systemd service on host system)", data_defaults=DEFAULTS)
def tunnel_ssh_client(
    tunnels,
    local_ssh_tunnel_private_key_file,
    tunnel_ssh_server,
    tunnel_ssh_server_port=2222,
    host=None,
    state=None,
):

    script_path = "/sbin/tunnel-ssh-client"
    ssh_private_key = "/root/.ssh/id_tunnel.private.key"
    service_name = "tunnel-client"
    private_key = files.put(
        name="Update private key",
        src=local_ssh_tunnel_private_key_file,
        dest=ssh_private_key,
        user="root",
        group="root",
        mode=600,
        add_deploy_dir=True,
        create_remote_dir=True,
        force=False,
        assume_exists=False,
        host=host,
        state=state,
    )
    files.template(
        name=f"Tunnel ssh client script",
        src=get_path("templates", "tunnel-ssh-client.sh.j2"),
        dest=script_path,
        user="root",
        group="root",
        mode="700",
        create_remote_dir=False,
        sudo=True,
        ssh_tunnel_private_key=ssh_private_key,
        tunnels=tunnels,
        tunnel_ssh_server=tunnel_ssh_server,
        tunnel_ssh_server_port=tunnel_ssh_server_port,
        host=host,
        state=state,
    )
    
    service_unit = files.template(
        name=f"Configure systemd {service_name}.service",
        src=get_path("templates", "tunnel-ssh-client.service.j2"),
        dest=f"/etc/systemd/system/{service_name}.service",
        user="root",
        group="root",
        mode="600",
        create_remote_dir=True,
        sudo=True,
        service_name=service_name,
        script_path=script_path,
        host=host,
        state=state,
    )
    systemd.service(
        name=f"Ensure {service_name}.service is enabled and running",
        service=f"{service_name}.service",
        running=True,
        # reloaded not applicable for *.timer unit use restart instead
        # reloaded=timer_unit.changed or service_unit.changed,
        restarted=service_unit.changed or private_key.changed,
        command=None,
        enabled=True,
        daemon_reload=service_unit.changed,
        sudo=True,
        host=host,
        state=state,
    )



