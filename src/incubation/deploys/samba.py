from os import path
from io import StringIO
from pyinfra.api import deploy, DeployError
from typing import List

from pyinfra.operations import systemd, apt, files
from incubation.operations import scaleway

from incubation.models.smb import SambaShare
from . import get_path

DEFAULTS = {}


@deploy("Install and configure samba", data_defaults=DEFAULTS)
def samba(
    shares: List[SambaShare]=None,
    state=None,
    host=None,
):
    # assume apt list is updated
    # https://wiki.samba.org/index.php/Distribution-specific_Package_Installation#Debian
    apt.packages(
        name="Ensure samba is installed",
        packages=[
            "acl",
            "attr",
            "samba",
            "samba-dsdb-modules",
            "samba-vfs-modules",
            "winbind",
            "libpam-winbind",
            "libnss-winbind",
            "krb5-config",
            "krb5-user",
            "dnsutils",
        ],
        state=state,
        host=host,
    )

    share_conf_directory = files.directory(
        name='Ensure /etc/samba/share.d/ directory',
        path="/etc/samba/share.d/",
        recursive=True,
        host=host,
        state=state,
    )
    smb_share_config_path = "/etc/samba/share.d/shares.conf"
    include_instruction = f"include = {smb_share_config_path}"
    samba_shares = files.template(
        name="Samba shares config",
        src=get_path("templates", "samba-shares.conf.j2"),
        dest=smb_share_config_path,
        create_remote_dir=False,
        share_list=shares,
        host=host,
        state=state,
    )
    for share in shares:
        files.directory(
            name=f'Ensure shared directory {share.part_name} exists',
            path=share.path,
            group=share.force_group if share.force_group else None,
            recursive=True,
            host=host,
            state=state,
        )
    samba_include_config = files.line(
        path="/etc/samba/smb.conf",
        line=include_instruction,
        present=True,
        replace=include_instruction,
        host=host,
        state=state,
    )

    systemd.service(
        name=f"Ensure samba.service is enabled and running",
        service="smbd.service",
        running=True,
        restarted=samba_shares.changed or samba_include_config.changed,
        command=None,
        enabled=True,
        sudo=True,
        host=host,
        state=state,
    )
