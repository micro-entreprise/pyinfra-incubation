from datetime import datetime
from pyinfra.api import deploy, DeployError
from incubation.models.user import User
from incubation.models.volume import Volume
from incubation.operations import scaleway
from pyinfra.operations import files, server
from incubation.operations import s3
from . import get_path

from os import path


DEFAULTS = {
    "anyblok_postgres_image": "postgres",
    "anyblok_postgres_image_version": "13",
    "anyblok_grafana_image": "grafana/grafana",
    "anyblok_grafana_image_version": "8.1.1",
    "anyblok_frontend_image": "caddy",
    "anyblok_frontend_image_version": "2",
    # change me late to anyblok as default value
    "anyblok_backend_alias": "stock-operator-backend",
    "anyblok_image": "anyblok",
    "anyblok_image_version": "14.0",
    "anyblok_pg_password": None,
    "anyblok_pg_user": "anyblok",
    "anyblok_pg_db": "anyblok",
    "anyblok_setup_bloks": "stock_operator_http_api",
    # refer to https://grafana.com/docs/grafana/latest/administration/configuration/#smtp
    "anyblok_grafana_smtp_password": None,
    "anyblok_grafana_smtp_from_address": None,
    "anyblok_grafana_smtp_from_name": None,
    "anyblok_grafana_smtp_host": None,
    "anyblok_grafana_smtp_user": None,
    "anyblok_grafana_smtp_password": None,
    "anyblok_grafana_smtp_starttls_policy": None,
    # according session backend used this param is useless
    # used by anyblok fastapi
    "anyblok_backend_session_secret_key": None,
}


@deploy("Install Anyblok", data_defaults=DEFAULTS)
def install_anyblok(
    volume: Volume,
    domain: str,
    grafana_domain: str,
    grafana_ldap_host: str,
    grafana_bind_dn: str,
    grafana_bind_dn_password: str,
    grafana_base_dn: str,
    grafana_search_user_filter: str,
    s3_backup: str = None,
    s3_config_file: str = None,
    s3_access_key: str = None,
    s3_secret_key: str = None,
    s3_host: str = None,
    s3_host_bucket: str = None,
    cups_required: bool = True,
    state=None,
    host=None,
):
    """ Automat anyblok deployements
    """
    if not host.data.anyblok_pg_password:
        raise DeployError("You must define anyblok_pg_password password")

    scaleway.mount(
        name=f"Mount {volume.scw_volume_name} partition " f"to {volume.mount_path}",
        volume=volume,
        host=host,
        state=state,
    )

    wd = volume.mount_path
    compose = path.join(wd, "docker-compose.yml")
    anyblok_conf = path.join(wd, "anyblok.cfg")
    grafana_ldap_conf = path.join(wd, "grafana_ldap.toml")


    config_file = files.template(
        name=f"Anyblok config",
        src=get_path("templates", "anyblok.cfg.j2"),
        dest=anyblok_conf,
        create_remote_dir=False,
        anyblok_pg_password=host.data.anyblok_pg_password,
        anyblok_pg_user=host.data.anyblok_pg_user,
        anyblok_pg_db=host.data.anyblok_pg_db,
        bloks=host.data.anyblok_setup_bloks,
        host=host,
        state=state,
    )

    files.directory(
        name='Ensure ./grafana_data ownership',
        path=path.join(wd, "grafana_data"),
        recursive=True,
        user="472",
        group="472",
        host=host,
        state=state,
    )
    grafana_ldap_config = False
    if grafana_bind_dn_password:
        grafana_ldap_config = files.template(
            name=f"Grafana ldap config file",
            src=get_path("templates", "anyblok_grafana_ldap.toml.j2"),
            dest=grafana_ldap_conf,
            create_remote_dir=False,
            ldap_host=grafana_ldap_host,
            bind_dn=grafana_bind_dn,
            bind_dn_password=grafana_bind_dn_password,
            base_dn=grafana_base_dn,
            search_user_filter=grafana_search_user_filter,
            host=host,
            state=state,
        ).changed
    compose_file = files.template(
        name=f"Compose file anyblok",
        src=get_path("templates", "anyblok.compose.j2"),
        dest=compose,
        create_remote_dir=False,
        domain=domain,
        anyblok_image=host.data.anyblok_image,
        anyblok_image_version=host.data.anyblok_image_version,
        anyblok_frontend_image=host.data.anyblok_frontend_image,
        anyblok_frontend_image_version=host.data.anyblok_frontend_image_version,
        backend_alias=host.data.anyblok_backend_alias,
        postgres_image=host.data.anyblok_postgres_image,
        postgres_image_version=host.data.anyblok_postgres_image_version,
        anyblok_pg_password=host.data.anyblok_pg_password,
        anyblok_pg_user=host.data.anyblok_pg_user,
        anyblok_pg_db=host.data.anyblok_pg_db,
        backend_session_secret_key=host.data.anyblok_backend_session_secret_key,
        grafana_image=host.data.anyblok_grafana_image,
        grafana_image_version=host.data.anyblok_grafana_image_version,
        grafana_domain=grafana_domain,
        grafana_ldap_enabled=True if grafana_bind_dn_password else False,
        grafana_smtp_enabled=True if host.data.anyblok_grafana_smtp_password else False,
        grafana_smtp_from_address=host.data.anyblok_grafana_smtp_from_address,
        grafana_smtp_from_name=host.data.anyblok_grafana_smtp_from_name,
        grafana_smtp_host=host.data.anyblok_grafana_smtp_host,
        grafana_smtp_user=host.data.anyblok_grafana_smtp_user,
        grafana_smtp_password=host.data.anyblok_grafana_smtp_password,
        grafana_smtp_starttls_policy=host.data.anyblok_grafana_smtp_starttls_policy,
        # GF_SMTP_SKIP_VERIFY	false
        users=host.data.anyblok_basic_auth_users,
        cups_required=cups_required,
        host=host,
        state=state,
    )

    initialized_databases = path.join(wd, ".initialized_databases")
    # TODO instead reading databases file ensure ir_config_parameter exists
    # in the database (consider starting stack if docker compose is present)
    # data could be present in volume...
    fresh_install = True
    try:
        lines = host.fact.find_in_file(initialized_databases, host.data.anyblok_pg_db)
        if lines:
            fresh_install = False
    except:
        pass

    
    if compose_file.changed or fresh_install or s3_backup:
        server.shell(
            name="Docker compose pull",
            commands=[f"docker compose -f {compose} pull"],
            host=host,
            state=state,
        )

        if s3_backup:
            host_path=path.join(wd, "pg_dumps/db_to_restore.dump")
            ct_path=path.join("/pg_dumps/", path.basename(host_path))
            
            files.directory(
                name='Ensure /pg_dumps exists',
                path=path.dirname(host_path),
                recursive=True,
                host=host,
                state=state,
            )
            s3.get(
                s3_backup,
                host_path,
                s3_access_key=s3_access_key,
                s3_secret_key=s3_secret_key,
                s3_host=s3_host,
                s3_host_bucket=s3_host_bucket,
                s3_config_file=s3_config_file,
                user=None,
                group=None,
                mode=None,
                # 6h
                cache_time=6 * 3600,
                force=False, 
                sha256sum=None,
                sha1sum=None,
                md5sum=None,
                state=state,
                host=host, 
            )
        # TODO: up maintenance
        server.shell(
            name="Docker compose down",
            commands=[f"docker compose -f {compose} down"],
            host=host,
            state=state,
        )
        if volume.scw_project_id and volume.scw_volume_id:
            volume.create_snapshot(
                host.data.scw_client,
                f"{datetime.now().isoformat()} - Before deploy anyblok"
            )
        else:
            print("do not snapshot volume, missing settings launch backup somehow!")
            # TODO find a proper way to do that properly calling only one db
            server.shell(
                name="Backup all database because missing volume settings",
                commands=["systemctl start archiver-daily-dump.service"],
                host=host,
                state=state,
            )
        server.shell(
            name="Start database",
            commands=[f"docker compose -f {compose} up -d db"],
            host=host,
            state=state,
        )
        server.shell(
            name="Wait database",
            commands=[
                f"docker compose -f {compose} exec -T db bash -c \""
                f"while ! psql -U {host.data.anyblok_pg_user} "
                f"-d empty_do_not_remove -c "
                f"'select 1' ; do echo 'Waiting pgsql init...'; sleep 1; done;\""
            ],
            host=host,
            state=state,
        )
        if fresh_install and not s3_backup:
            server.shell(
                name="Install anyblok",
                commands=[
                    f"docker compose -f {compose} run --rm anyblok bash -c "
                    f"'anyblok_createdb -c /etc/anyblok.cfg'"
                ],
                host=host,
                state=state,
            )
        else:
            if s3_backup:
                server.shell(
                    name="restore dump",
                    commands=[
                        "docker compose "
                        f"-f {compose} " 
                        "exec -T db bash "
                        '-c "'
                        f"dropdb -U {host.data.anyblok_pg_user} {host.data.anyblok_pg_db}; "
                        f"createdb -U {host.data.anyblok_pg_user} {host.data.anyblok_pg_db} && "
                        f"pg_restore -U {host.data.anyblok_pg_user} -Fc -O -d {host.data.anyblok_pg_db} {ct_path} "
                        '"'
                    ],
                    host=host,
                    state=state,
                )
                # TODO allow to run SQL file
            # TODO remove assets bundel
            # delete from ir_attachment where res_model = 'ir.ui.view' and (name like '%.assets_%.js' or name like '%.assets_%.css');
            server.shell(
                name="Update anyblok",
                commands=[
                    f"docker compose -f {compose} run --rm anyblok bash -c "
                    f"'anyblok_updatedb -c /etc/anyblok.cfg --update-all-bloks'",
                ],
                host=host,
                state=state,
            )

        files.line(
            name="initialyzed database file",
            path=initialized_databases,
            line=host.data.anyblok_pg_db,
            host=host,
            state=state,
        )

        server.shell(
            name="Start anyblok stack",
            commands=[f"docker compose -f {compose} up -d"],
            host=host,
            state=state,
        )
        # TODO: down maintenance

        # TODO: recovery
            # restore previous snapshot

    else:
        server.shell(
            name="Docker compose up",
            commands=[
                f"docker compose -f {compose} up -d "
                f"{'--force-recreate' if config_file.changed or grafana_ldap_config else ''}"
            ],
            host=host,
            state=state,
        )
        
