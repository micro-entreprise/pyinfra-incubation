from os import path

from pyinfra.api import deploy, DeployError
from pyinfra.operations import files, server

from incubation.operations import scaleway
from incubation.models.volume import Volume
from . import get_path


DEFAULTS = {
    "traefik_network": "traefik",  # docker published container network
    "traefik_acme_email": None,  # LE acme registered email
    "le_acme_server": "https://acme-staging-v02.api.letsencrypt.org/directory",
    # "le_acme_server": "https://acme-v02.api.letsencrypt.org/directory",
    "traefik_api_users": None,  # traefik dashboard/api basic_auth_bcrypt users
    "traefik_image": "traefik",  # docker image
    "traefik_image_version": "latest",  # docker image tag
    "traefik_metrics_prometheus_domain": None,
    "traefik_metrics_prometheus_users": None,
    "authelia_image": "authelia/authelia",
    "authelia_image_version": "4.38.17",
    "authelia_tz": "Europe/Paris",
    "authelia_access_control": "# https://www.authelia.com/docs/configuration/access-control.html",
    "authelia_ldap_server": None,
    "authelia_ldap_base_dn": None,
    "authelia_ldap_user": None,
    "authelia_ldap_password": None,
    "authelia_default_redirection_url": "",
    "authelia_storage_secret": "authelia-storage-secret",
    "authelia_jwt_secret": "PleaseOverwriteMeWithSomehingStronger!",
    "authelia_storage_ecnryption_key": "You-should-change-this-value-to-enforce-security",
}
# import os
# from pyinfra import host
# from incubation.operations.scaleway import mount

@deploy("Install Treafik", data_defaults=DEFAULTS)
def install_traefik(
    volume: Volume,
    domain: str,
    login_domain: str=None,
    traefik_redirections_conf_file: str = None,
    state=None,
    host=None,
):

    with_authelia = True
    if not login_domain:
        with_authelia = False

    if not host.data.traefik_api_users:
        raise DeployError("You must define traefik_api_users")

    if not host.data.traefik_acme_email:
        raise DeployError(
            "You must define traefik_acme_email in order to "
            "properly use LE certificates"
        )

    scaleway.mount(
        name=f"Mount {volume.scw_volume_name} partition " f"to {volume.mount_path}",
        volume=volume,
        host=host,
        state=state,
    )

    wd = volume.mount_path
    compose = path.join(wd, "docker-compose.yml")
    conf = path.join(wd, "traefik.toml")
    authelia_conf = path.join(wd, "authelia.yml")
    class FakeChanged:
        changed = False
    
    redirection_config_file = FakeChanged()

    has_redirections = False
    if traefik_redirections_conf_file:
        has_redirections = True
        redirections_conf = path.join(wd, "redirections.toml")
        redirection_config_file = files.put(
            name="Update traefik redirections files",
            src=traefik_redirections_conf_file,
            dest=redirections_conf,
            user="root",
            group="root",
            mode=600,
            add_deploy_dir=True,
            create_remote_dir=True,
            force=False,
            assume_exists=False,
            host=host,
            state=state,
        )

    if "traefik" not in [net.get("Name", "") for net in host.fact.docker_networks]:
        server.shell(
            name=f"Create traefik network {host.data.traefik_network}",
            commands=[f"docker network create --attachable {host.data.traefik_network}"],
            host=host,
            state=state,
        )

    config_file = files.template(
        name=f"traefik2 config",
        src=get_path("templates","traefik.toml.j2"),
        dest=conf,
        user="root",
        group="root",
        mode="600",
        has_redirections=has_redirections,
        le_acme_server=host.data.le_acme_server,
        traefik_network=host.data.traefik_network,
        traefik_acme_email=host.data.traefik_acme_email,
        enable_prometheus=host.data.traefik_metrics_prometheus_domain,
        host=host,
        state=state,
    )

    authelia_config_file = FakeChanged()
    if with_authelia:
        authelia_config_file = files.template(
            name=f"Authelia config",
            src=get_path("templates", "traefik-authelia.yml.j2"),
            dest=authelia_conf,
            create_remote_dir=False,
            default_redirection_url=host.data.authelia_default_redirection_url,
            storage_ecnryption_key=host.data.authelia_storage_ecnryption_key,
            access_control=host.data.authelia_access_control,
            ldap=host.data.authelia_ldap_server,
            base_dn=host.data.authelia_ldap_base_dn,
            ldap_user=host.data.authelia_ldap_user,
            ldap_password=host.data.authelia_ldap_password,
            storage_secret=host.data.authelia_storage_secret,
            jwt_secret=host.data.authelia_jwt_secret,
            authelia_url=f"https://{login_domain}",
            authelia_protected_domain=".".join(login_domain.split(".")[-2:]),
            smtp_user=host.data.authelia_smtp_user,
            smtp_password=host.data.authelia_smtp_password,
            smtp_host=host.data.authelia_smtp_host,
            smtp_port=host.data.authelia_smtp_port,
            smtp_sender=host.data.authelia_smtp_sender,
            host=host,
            state=state,
        )

    files.directory(
        name="cert acme directory is present",
        path=path.join(wd, "certs"),
        present=True,
        assume_present=False,
        user="root",
        group="root",
        mode="600",
        recursive=False,
        host=host,
        state=state,
    )

    image = host.data.traefik_image
    version = host.data.traefik_image_version
    users = host.data.traefik_api_users

    compose_file = files.template(
        name=f"Compose file: reverse proxy (traefik 2)",
        src=get_path("templates", "traefik-compose.yml.j2"),
        dest=compose,
        user="root",
        group="root",
        mode="600",
        create_remote_dir=False,
        image=image,
        version=version,
        domain=domain,
        users=users,
        traefik_network=host.data.traefik_network,
        has_redirections=has_redirections,
        prometheus_domain=host.data.traefik_metrics_prometheus_domain,
        prometheus_users=host.data.traefik_metrics_prometheus_users,
        with_authelia=with_authelia,
        authelia_image=host.data.authelia_image,
        authelia_image_version=host.data.authelia_image_version,
        authelia_login_domain=login_domain,
        authelia_storage_secret=host.data.authelia_storage_secret,
        authelia_tz=host.data.authelia_tz,
        host=host,
        state=state,
    )
    present_images = [
        img for inspect in host.fact.docker_images for img in inspect.get("RepoTags", [])
    ]

    new_images = server.shell(
        name="Pull latest images",
        commands=[f"docker compose -f {compose} pull"],
        host=host,
        state=state,
    )
    force_recreate = compose_file.changed or config_file.changed or authelia_config_file.changed or redirection_config_file.changed
    up_service = server.shell(
        name="Docker compose up",
        commands=[f"docker compose -f {compose} up -d {'--force-recreate' if force_recreate else ''}"],
        host=host,
        state=state,
    )
    # TODO: assert service is up and running
