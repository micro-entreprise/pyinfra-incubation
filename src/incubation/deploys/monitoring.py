from pyinfra.api import deploy, DeployError
from pyinfra.operations import files, server
from incubation.models.volume import Volume
from incubation.operations import scaleway
from . import get_path

from os import path


DEFAULTS = {
    "monitoring_prometheus_users": None,
    "monitoring_pushgateway_users": None,
    "monitoring_postgres_image": "postgres",
    "monitoring_postgres_image_version": "14",
    "monitoring_grafana_image": "grafana/grafana",
    "monitoring_grafana_image_version": "8.5.2",
    # refer to https://grafana.com/docs/grafana/latest/administration/configuration/#smtp
    "monitoring_smtp_from_address": None,
    "monitoring_smtp_from_name": None,
    "monitoring_smtp_host": None,
    "monitoring_smtp_user": None,
    "monitoring_smtp_password": None,
    "monitoring_smtp_starttls_policy": None,
    "monitoring_grafana_ldap_host": None,
    "monitoring_grafana_bind_dn": None,
    "monitoring_grafana_bind_dn_password": None,
    "monitoring_grafana_base_dn": None,
    "monitoring_grafana_search_user_filter": None,
    "monitoring_grafana_admin_ldap_group": None,
    "monitoring_grafana_editor_ldap_group": None,
    "monitoring_grafana_viewer_ldap_group": None,
}



@deploy("Install Monitoring: prometheus/grafana", data_defaults=DEFAULTS)
def install_monitoring(
    volume: Volume,
    prometheus_domain: str,
    alertmanager_domain: str,
    pushgateway_domain: str,
    grafana_domain: str,
    state=None,
    host=None,
):

    scaleway.mount(
        name=f"Mount {volume.scw_volume_name} partition " f"to {volume.mount_path}",
        volume=volume,
        host=host,
        state=state,
    )

    wd = volume.mount_path
    compose = path.join(wd, "docker-compose.yml")
    grafana_ldap_conf = path.join(wd, "grafana_ldap.toml")
    
    grafana_ldap_config = False
    if host.data.monitoring_grafana_bind_dn_password:
        grafana_ldap_config = files.template(
            name=f"Grafana ldap config file",
            src=get_path("templates", "monitoring_grafana_ldap.toml.j2"),
            dest=grafana_ldap_conf,
            create_remote_dir=False,
            ldap_host=host.data.monitoring_grafana_ldap_host,
            bind_dn=host.data.monitoring_grafana_bind_dn,
            bind_dn_password=host.data.monitoring_grafana_bind_dn_password,
            base_dn=host.data.monitoring_grafana_base_dn,
            search_user_filter=host.data.monitoring_grafana_search_user_filter,
            admin_ldap_group=host.data.monitoring_grafana_admin_ldap_group,
            editor_ldap_group=host.data.monitoring_grafana_editor_ldap_group,
            viewer_ldap_group=host.data.monitoring_grafana_viewer_ldap_group,
            host=host,
            state=state,
        ).changed
    
    compose_file = files.template(
        name=f"Compose file monitoring stack",
        src=get_path("templates", "monitoring.compose.j2"),
        dest=compose,
        create_remote_dir=False,
        prometheus_domain=prometheus_domain,
        prometheus_users=host.data.monitoring_prometheus_users,
        alertmanager_domain=alertmanager_domain,
        pushgateway_domain=pushgateway_domain,
        pushgateway_users=host.data.monitoring_pushgateway_users,
        grafana_image=host.data.monitoring_grafana_image,
        grafana_image_version=host.data.monitoring_grafana_image_version,
        grafana_domain=grafana_domain,
        grafana_ldap_enabled=True if host.data.monitoring_grafana_bind_dn_password else False,
        grafana_smtp_enabled=True if host.data.monitoring_smtp_password else False,
        grafana_smtp_from_address=host.data.monitoring_smtp_from_address,
        grafana_smtp_from_name=host.data.monitoring_smtp_from_name,
        grafana_smtp_host=host.data.monitoring_smtp_host,
        grafana_smtp_user=host.data.monitoring_smtp_user,
        grafana_smtp_password=host.data.monitoring_smtp_password,
        grafana_smtp_starttls_policy=host.data.monitoring_smtp_starttls_policy,
        # GF_SMTP_SKIP_VERIFY	false
        postgres_image=host.data.monitoring_postgres_image,
        postgres_image_version=host.data.monitoring_postgres_image_version,
        host=host,
        state=state,
    )

    server.shell(
        name="Docker compose up",
        commands=[
            f"docker compose -f {compose} up -d "
            f"{'--force-recreate' if compose_file.changed or grafana_ldap_config else ''}"
        ],
        host=host,
        state=state,
    )
    
NETDATA_DEFAULT={

}
@deploy("Install netdata", data_defaults=NETDATA_DEFAULT)
def netdata(
    state=None,
    host=None,
):
    # TODO: restrict access
    # getting current config
    # ssh -L19999:localhost:19999 scw-cgi-recette-1 -qN
    # http://localhost:19999/netdata.conf

    # Restricts like this?
    # [web]
    #   mode = static-threaded
    #   allow connections from = localhost 10.70.191.*
    #   allow dashboard from = localhost

    # allow netdata to gest cgroup informations
    # do it manualy for the time being
    # https://learn.netdata.cloud/docs/agent/collectors/cgroups.plugin
    # #how-to-enable-cgroup-accounting-on-systemd-systems-that-is-by-default-disabled
    server.shell(
        name="install or udpate netdata",
        commands=["wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh --disable-cloud --stable-channel --disable-telemetry"],
        host=host,
        state=state,
    )
    # files.template(
    #         name=f"Nedata config file",
    #         src=get_path("templates", "monitoring.netdata.conf.j2"),
    #         dest="/etc/netdata/netdata.conf",
    #         create_remote_dir=True,
    #         host=host,
    #         state=state,
    # )