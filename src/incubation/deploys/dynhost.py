"""This state configure a DynHost timer

DynHost is common name were you provider gives a dynamic
address IP. In that case you have to configure a DNS
that you'll dynamically update once your IP has been changed.

This operation add a systemd timer on your system
"""


from os import path

from pyinfra.api import deploy, DeployError
from pyinfra.operations import files, systemd

from . import get_path

DEFAULTS = {
    "dynhost_service_name_prefix": "dynhost",
    # every minute
    "dynhost_on_calendar": "*-*-* *:*:00",
}


@deploy("Install dynhost unit and server", data_defaults=DEFAULTS)
def dynhost(
    dyn_domain: str,
    dyn_user: str,
    dyn_password: str,
    host=None,
    state=None,
):
    service_name = f"{host.data.dynhost_service_name_prefix}-{dyn_domain}"

    files.template(
        name=f"DynHost .sh executable {service_name}",
        src=get_path("templates", "dynhost.j2"),
        dest=f"/sbin/dynhost",
        user="root",
        group="root",
        mode="700",
        create_remote_dir=False,
        sudo=True,
        service_name=service_name,
        dyn_domain=dyn_domain,
        dyn_user=dyn_user,
        dyn_password=dyn_password,
        host=host,
        state=state,
    )
    service_unit = files.template(
        name=f"Configure systemd {service_name}.service",
        src=get_path("templates", "dynhost.service.j2"),
        dest=f"/etc/systemd/system/{service_name}.service",
        user="root",
        group="root",
        mode="755",
        create_remote_dir=True,
        sudo=True,
        service_name=service_name,
        dyn_domain=dyn_domain,
        dyn_user=dyn_user,
        dyn_password=dyn_password,
        host=host,
        state=state,
    )
    timer_unit = files.template(
        name=f"Configure systemd {service_name}.timer",
        src=get_path("templates", "dynhost.timer.j2"),
        dest=f"/etc/systemd/system/{service_name}.timer",
        user="root",
        group="root",
        mode="755",
        create_remote_dir=True,
        sudo=True,
        service_name=service_name,
        dyn_domain=dyn_domain,
        on_calendar=host.data.dynhost_on_calendar,
        host=host,
        state=state,
    )
    systemd.service(
        name=f"Ensure {service_name}.timer is enabled and running",
        service=f"{service_name}.timer",
        running=True,
        # reloaded not applicable for *.timer unit use restart instead
        # reloaded=timer_unit.changed or service_unit.changed,
        restarted=timer_unit.changed or service_unit.changed,
        command=None,
        enabled=True,
        daemon_reload=timer_unit.changed or service_unit.changed,
        sudo=True,
        host=host,
        state=state,
    )
