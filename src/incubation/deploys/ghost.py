from datetime import datetime
from pyinfra.api import deploy, DeployError
from incubation.models.user import User
from incubation.models.volume import Volume
from incubation.operations import scaleway
from pyinfra.operations import files, server
from . import get_path

from os import path


DEFAULTS = {
    "ghost_image": "ghost",
    "ghost_image_version": "4.34.3",
}


@deploy("Install Ghost", data_defaults=DEFAULTS)
def install_ghost(
    volume: Volume,
    domain: str,
    database_name: str = None,
    database_root_pwd: str = None,
    database_user: str = None,
    database_password: str = None,
    ghost_options: dict = None,
    ghost_basic_auth: str = None,
    state=None,
    host=None,
):
    """ Automat odoo deployements
    """
    if not ghost_options:
        ghost_options = dict()

    if not database_name:
        database_name = domain
   
    if not database_user:
        database_name = domain

    if not database_password:
        raise DeployError("You must define database_password for msql")

    scaleway.mount(
        name=f"Mount {volume.scw_volume_name} partition " f"to {volume.mount_path}",
        volume=volume,
        host=host,
        state=state,
    )

    wd = volume.mount_path
    compose = path.join(wd, "docker-compose.yml")

    compose_file = files.template(
        name=f"Compose file Odoo",
        src=get_path("templates", "ghost.compose.j2"),
        dest=compose,
        create_remote_dir=False,
        domain=domain,
        ghost_image=host.data.ghost_image,
        ghost_image_version=host.data.ghost_image_version,
        database_name=database_name,
        database_root_pwd=database_root_pwd,
        database_user=database_user,
        database_password=database_password,
        ghost_options=ghost_options,
        ghost_basic_auth=ghost_basic_auth,
        host=host,
        state=state,
    )
    if compose_file.changed:
        volume.create_snapshot(
            host.data.scw_client,
            f"{datetime.now().isoformat()} - Before deploy ghost"
        )
    server.shell(
        name="Docker compose pull",
        commands=[f"docker compose -f {compose} pull"],
        host=host,
        state=state,
    )
    server.shell(
        name="Docker compose up",
        commands=[f"docker compose -f {compose} up -d {'--force-recreate' if compose_file.changed else ''}"],
        host=host,
        state=state,
    )
        